//----------------------------------------------------------------------------------------------------------------------
// DocsConfig
//----------------------------------------------------------------------------------------------------------------------

module.exports = {
    siteConfig: {
        title: 'RFI:Precursors Docs',
        description: 'Documentation for RFI:Precursors.',
        lang: 'en'
    },
    themeConfig: {
        enableEditing: true,
        projectURL: 'https://gitlab.com/skewed-aspect/games/precursors/docs'
    },
    theme: './theme',
    plugins: [ 'saber-plugin-prismjs' ],
    markdown: {
        highlighter: 'prism',
        headings: { permalink: true }
    },
    build: {
        publicUrl: '/games/precursors/docs',
        outDir: './public'
    }
};

//----------------------------------------------------------------------------------------------------------------------
