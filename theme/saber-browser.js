//----------------------------------------------------------------------------------------------------------------------
// Saber Browser API
//----------------------------------------------------------------------------------------------------------------------

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import vSelect from 'vue-select';

Vue.config.ignoredElements = [ 'eq', 'eqn' ];

//-----------------------------------------------------------------------------------------------------------------------
// Misc Style Imports
//-----------------------------------------------------------------------------------------------------------------------

import 'prismjs/themes/prism-coy.css';
import 'katex/dist/katex.min.css';

//-----------------------------------------------------------------------------------------------------------------------
// Vue Bootstrap
//-----------------------------------------------------------------------------------------------------------------------

import './style/theme.scss';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);

//----------------------------------------------------------------------------------------------------------------------
// Vue Select
//----------------------------------------------------------------------------------------------------------------------

import 'vue-select/dist/vue-select.css';

// Set the components prop default to return our fresh components
vSelect.props.components.default = () => ({
    OpenIndicator: {
        render() {}
    }
});

Vue.component('v-select', vSelect);

//----------------------------------------------------------------------------------------------------------------------
// Font Awesome
//----------------------------------------------------------------------------------------------------------------------

// Font Awesome
import { library, config } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome';

// If we let it add the CSS, it's done too late, so the icons flash huge for a second.
config.autoAddCss = false;

library.add(fab, fas);
Vue.component('fa', FontAwesomeIcon);
Vue.component('fa-layers', FontAwesomeLayers);

//----------------------------------------------------------------------------------------------------------------------
// Build Theme Mixin
//----------------------------------------------------------------------------------------------------------------------

import { pages } from 'saber/variables';

Vue.mixin({
    computed: {
        $pages() { return pages; },
        $categories()
        {
            return pages.reduce((results, page) =>
            {
                const category = page.category;
                if(category)
                {
                    const pages = results[category] || [];
                    pages.push(page);
                    results[category] = pages;
                } // end if

                return results;
            }, {});
        }
    }
});

//----------------------------------------------------------------------------------------------------------------------
