//----------------------------------------------------------------------------------------------------------------------
// Saber Node API
//----------------------------------------------------------------------------------------------------------------------

const katex = require('katex');

const emoji = require('markdown-it-emoji');
const plantuml = require('markdown-it-plantuml');
const texmath = require('markdown-it-texmath');

//----------------------------------------------------------------------------------------------------------------------

function defineVariables(variables)
{
    return Object.assign(variables, {
        pages: Array
            .from(this.pages.values())
            .map((page) =>
            {
                // eslint-disable-next-line no-unused-vars
                const { attributes, ...cleanPage } = page;
                return cleanPage;
            })
    });
} // end defineVariables

function onCreatePages()
{
    // Add Sitemap page
    this.pages.createPage({
        internal: {
            id: `sitemap-${ Date.now() }`
        },
        title: 'Sitemap',
        slug: 'sitemap',
        permalink: '/sitemap',
        layout: 'sitemap'
    });
} // end onCreatePages

function chainMarkdown(config)
{
    config.plugin('armdocs')
        .use((md) =>
        {
            md.use(emoji);
            md.use(plantuml, { openMarker: '```plantuml', closeMarker: '```', format: 'svg' });
            md.use(texmath.use(katex), { delimiters: 'gitlab' });

            //----------------------------------------------------------------------------------------------------------
            // Custom Render Rules
            //----------------------------------------------------------------------------------------------------------

            // eslint-disable-next-line camelcase
            md.renderer.rules.table_open = function()
            {
                return '<div class="card drop-shadow mb-2"><table class="table table-striped">';
            };

            // eslint-disable-next-line camelcase
            md.renderer.rules.table_close = function()
            {
                return '</table></div>';
            };
        });
} // end chainMarkdown

//----------------------------------------------------------------------------------------------------------------------

module.exports = {
    defineVariables,
    onCreatePages,
    chainMarkdown
}; // end exports

//----------------------------------------------------------------------------------------------------------------------
