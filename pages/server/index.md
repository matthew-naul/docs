---
title: 'Server Architecture'
layout: default
sidebar: true
---

# Architecture

All modern MMOs do not have a single 'server', and Precursors will be no different. However, the problem a lot of MMOs have is that while they may have split the server codebase, they still treat it like a single logical server. In this day and age of ['SaaS'][saas], ['N-tier'][ntier] and ['Serverless'][serverless], it seems clear that the industry has decided on a direction to scale software, and monolithic architectures **are not it**.

## Previous Attempts

In the past, our last monolithic server was technically the [Node.JS server][rfi-node-server], but it was architected to have multiple services coordinating with each other. That makes the [python server][rfi-py-server] the last time we had a truly monolithic architecture. The [Erlang server][rfi-erlang-server], thanks to using the proper paradigms for the language, has multiple 'applications' internal to the server, which could be scaled differently, and moved among running nodes.

The problem all of our architectures have suffered, however, is that they've been functionally decomposed, as opposed to being decomposed by volatility. Even worse, communication was not split at all, instead going through a single server before being distributed to other servers, regardless of if the message had to be handled in real-time or not.

[saas]: https://en.wikipedia.org/wiki/Software_as_a_service
[ntier]: https://en.wikipedia.org/wiki/Multitier_architecture
[serverless]: https://en.wikipedia.org/wiki/Serverless_computing

[rfi-node-server]: https://github.com/SkewedAspect/rfi-server
[rfi-py-server]: https://bitbucket.org/skewedaspect/precursors/src/default/
[rfi-erlang-server]: https://github.com/SkewedAspect/precursors-server

## Modern Structure

Given a modern architecture, we can make several immediate improvements and structure our architecture to scale as needed. This also benefits development and deployment; a large percentage of the server side code can be patched live, with no downtime. Additionally, we can leverage technologies like [kubernetes][] to achieve on-demand scaling and (with a little bit of work) live migration.

Here's the over all system diagram:

<div class="text-center mb-3">
    <img src="../../diagrams/images/PrecursorsArchitecture.svg" alt="Architecture">
</div>

Let's break this down. First, we need to establish some terms:

* **Zone** - A 3D scene that represents a group of physical entities that can interact.
* **Real Time Stack** - The pieces of the architecture that require near real-time performance.
* **Real Time Server** - An application with a direct connection to the client, responsible for handling Zone state.
* **Non-Real Time Stack** - The pieces of the architecture that can respond with up to several seconds of delay.
* **Services** - These are applications running with not direct communication with the outside, only receiving requests via a message queue. They read or update game state in some way.

[kubernetes]: https://kubernetes.io/

### The Clients

One of the immediate things this architecture lends itself towards (and the entire reason for choosing websockets for the Non-Real Time Stack) is having multiple clients logged in at once. We can support "3D" clients (such as a [Unity][unity] or [Godot][godot] client) as well as a "2D" client, like the main game website, or a [PWA][pwa]. This allows us to give players more access to (and spend more time "playing") parts of the game, even when they can't dedicate time in front of their gaming rig. What's more, we have a chance to enhance and deepen the experience in ways other games simply don't.

[unity]: https://unity.com/
[godot]: https://godotengine.org/
[pwa]: https://developers.google.com/web/progressive-web-apps/

### The Real Time Stack

The Real Time Stack is made up multiple Real Time Servers. Each server handles a single zone, and keeps that zone's state in it's own sqlite database. This state can be queried by services in the Non-Real Time Stack, and is eventually sync'd to the Game Store DB.

#### Real Time Server

The Real Time Server (RTS) will be written in a language that allows for concurrency, multi-core processing, and high performance calculation. This could be [Erlang][erlang], [Rust][rust], [Dart][dart], or even maybe [NodeJS][node]. This server will directly accept connections from the client.

[erlang]: https://www.erlang.org/
[rust]: https://www.rust-lang.org/
[dart]: https://dart.dev/
[node]: https://nodejs.org

Each RTS instance will handle a single Zone. Objects cannot interact across Zones, and a Zone will never cross multiple servers. This makes scaling a single Zone the single most difficult part of our stack, however, we can scale [vertically][vert-scale], and if we build the system right, it should be able to handle a sufficient number of concurrent players per zone.

##### The Rules of Zones

1. One Zone per RTS.
2. Entities in a Zone can interact **only** with other entities in the same Zone.
3. An entity moving between Zones is **never** in real time. (i.e. there's a loading screen or animation.)
4. Zones are managed in "soft" real time; it is the Non-Real Time stack that controls the create and removal of Zones
5. A Zone is removed once the last player controlled entity has left the Zone.

[vert-scale]: https://en.wikipedia.org/wiki/Scalability#Vertical

##### Communication Protocol

Per the diagram, TCP (TLS) is specified. It's commonly accepted that TCP isn't appropriate for real-time games. However, it's very difficult to secure, and since it's not a connection-based protocol, each datagram needs to have an id included to route the message. Practically, this means every datagram would need to be encrypted, with a symmetric cipher (for speed) and then have non-trivial route handing built.

Due to the complexity, we will prototype with TCP, and if it becomes an issue, we will move to something more performant, once we've positively identified the bottleneck.

##### Container

Deploying one RTS per container, we can limit the maximum CPU/Memory from the container to allow multiple to run safely on a single node. Then, as usage increases, we can preemptively detect the load, and migrate the zone to it's own node, as needed. If we do this correctly, we can spin up a new node in it's own container, share the sqlite database between the containers, and then redirect traffic to the new node, while the old finishes processing and shuts done.

If we build the system properly, we can easily "live-migrate" zones to larger, more impressive hardware, without a single hiccup in gameplay. All users will notice is that things suddenly get smoother. (If we tune our thresholds correctly they'd never notice degraded gameplay.) We can also not allow players to join zones that are at capacity; in the event there's a major event going on and everyone tries to log into the same Zone to participate, we can start denying people once we know we can't handle it. (That's preferable to making the game unplayable for those who've made it in.)

### The Non-Real Time Stack

This stack is build much like modern scalable web applications, and is in fact modeled after an enterprise-scale deployment. Because of this, a familiar technology stack will be preferred. While there's nothing wrong with the alternatives, this stack's quirks have been known and understood by the team over the last five years, and will definitely handle everything this project could throw at it.

#### Websocket Server

The Websocket Server (wsapi) is the gateway for all Non-Real Time communication. For this, we have a strong preference for socket.io, despite it limiting some possibilities for clients. If plain websockets are used, a similar communication protocol will need to be enforced, with both one way and two way (request/response) mechanisms. That protocol would need to be implemented by all clients. (It's easier to grab a working one off the shelf, however.)

The websocket server forwards all websocket requests to the message queue, posting messages by `context` and `operation` into queues and then waiting for a response. That response will be passed back along the websocket to fullfill the waiting request.

#### Services

Services will most likely be built with [Node.js][node], as we have a lot of experience scaling node, and development is very fast, and there's a lot of pre-built packages we can leverage. Regardless, services would be expected to be asynchronous, able to communicate with our message queue, and easy to wrap inside a docker.

These services will be able to call each other, as well as access the database directly. They should be split by logical job, volatility, and scaling considerations. For the moment, the recommendation would be an Auth/Security service, a Game service, and Zone service. These could be combined or split as required.

Services will communicate with the Game Store for most of their needs, however, if they need a piece of Zone data, and an entity is currently in a zone, then they will communicate directly with the RTS for that information.
