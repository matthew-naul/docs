---
title: Message Queue Protocol
layout: default
sidebar: true
---

# Message Queue Protocol

While having a message queue is required, it doesn't define what a message looks like. We have to have some standard, or routing messages (or matching up responses) is impossible. What follows is a modification of work done on a previous system, that has worked very well for over 5 years in production.

There are a few terms that are important to define here:

* **client** - software that is initiating either a `request` or `action`.
* **service** - software that is responding to a `request`, or receiving an `action`.

## A Note on Redis

This protocol was implemented with Redis in mind. Redis has a very rudimentary message queue implementation, meaning that read receipts and other niceties are not implemented. While this protocol is valid across any message queue, it's designed not to take advantage of features that might not be in some implementations.

Additionally, redis has a limitation of 512MB for a string.

## Requests

A 'request' is a message from a client that requires an acknowledgement. Making a request implies wanting a response of some kind (even a simple `'ok'`). (If you're not wanting an acknowledgement, then take a look at `action` below.)

A `request` **must always have a response**. Failure to respond should be treated by all clients as an error. (Given this, clients are _required_ to implement their own timeouts on requests. Reasonable timeouts should be implemented based on the request, thought long-running requests should be discouraged across an external network.)

A `request` actually has two pieces to it. The `Request Envelope`, and the `Response Envelope`.

### Request Envelope

| Field | Type | Required | Protocol | Semantics | Comments |
| ------ | ------ | ------ |:------:| ------ | ------ |
| **id** | `string(60)` | **required** | `*` | Must be a unique id, preferably a GUID/UUID. Assigned by communication library; should be treated as a unique, opaque value, and should be unique for all time. | This is for matching messages, and avoiding duplicates. |
| **protocol** | `string(20)` | _optional_ | `>=2.0.0` | A [semver][] version number, indicating the protocol version. If omitted, will be treated as `'1.0.0'` | This should allow for changes to the protocol in the future. |
| **context** | `string(255)` | **required** | `*` | A conceptual grouping of related operations. | This is most similar to a resource in REST terms. |
| **operation** | `string(255)` | **required** | `*` | The specific operation being requested. | This is most similar to an endpoint in REST terms. |
| **version** | `string(20)` | _optional_ | `>=2.0.0` | A [semver][] version number, indicating the desired version of the context/operation. If omitted, any version will be accepted. | Not all services support versioned endpoints. In that case, version specifiers are ignored. |
| **payload** | `object` | **required** | `*` | And object of arguments required by the operation. (Each request payload has it's own schema.) | Previous versions allowed this to be any valid type, but we have canonized it being an object, now. |
| **clientReference** | `string(255)` | _optional_ | `^1.0.0` | An opaque identifier for use by the client, only. | This would be the place for a client to store extra information needed to help handle/route a received response internally. |
| **responseQueue** | `string(255)` | **required** | `*` | The queue in which to place any response generated for a request. | Implementers are encouraged to put an expiration on these keys. Expiration length  |
| **userInfo** | `userInfo` | _optional_ | `^1.0.0` | Identifies a request as a specific user. | The `userToken` field should be preferred. |
| **userToken** | `jwt` | _optional_ | `>=2.0.0` | Identifies a request as a specific user. Must follow [RFC7519][rfc7519]. | This should be preferred. |
| **metadata** | `object` | _optional_ | `*` | Used to store meaningful, unschema'd tracking information. | We use this for developer-based tracking and analytics, but should never be used or relied on by code. |
| **timestamp** | `string(25)` | **required** | `*` | [ISO 8601][iso8601] Date format; e.g., `'2014-06-25T04:08:22Z'` |  |
| **timeout** | `int32` | _optional_ | `*` | In milliseconds; for how long the client is interested in receiving a response. The service may send a timeout response when this is exceeded. Defaults to zero or an internal, service-supplied timeout value if not provided. Sending a non-zero value will override a service’s default. | Clients should also consider having their own application timeouts as well. |
| **hostname** | `string(255)` | _optional_ | `^1.0.0` |  Identify the sending hostname of the client |  |
| **priorRequestID** | `string(60)` | _optional_ | `^1.0.0` | If this request was made in the course of serving another request then prior requests UUID should be included | This gives us a method of reconstructing the full stack of the requests that led to this one, allowing us to trace a given query and all of its effects through the system. |

#### `userInfo`

This identifies a user, and contains useful information about them.

| Field | Type | Required | Protocol | Semantics | Comments |
| ------ | ------ | ------ |:------:| ------ | ------ |
| **userName** | `string(255)` | **required** | `^1.0.0` | The username. | This field is used everywhere, in all our validations, but it's a bad practice, and poorly capitalized. |
| **email** | `string(255)` | _optional_ | `^1.0.0` | The user's email. Must be a valid email ([RFC2822][rfc2822] compliant). | This field is used everywhere, in all our validations, but it's a bad practice, and poorly capitalized. |
| **displayName** | `string(255)` | _optional_ | `^1.0.0` | The user's name for display purposes. | Used by most of our clients. |
| **credentialType** | `string(255)` | _optional_ | `^1.0.0` | Indicates where the credentials come from. | Currently, this is optional, though there is some code that uses it to check for a specific type of credential, and then performs additional checks. |

### Response Envelope

| Field | Type | Required | Protocol | Semantics | Comments |
| ------ | ------ | ------ |:------:| ------ | ------ |
| **id** | `string(60)` | **required** | `*` | Same as `request`. |  |
| **context** | `string(255)` | **required** | `*` | Same as `request`. |  |
| **operation** | `string(255)` | **required** | `*` | Same as `request`. |  |
| **payload** | `object` or `array` | **required** | `*` | An object or array representing the results of the operation. (Each response payload has its own schema.) | The only two things supported are objects and arrays. |
| **clientReference** | `string(255)` | _optional_ | `^1.0.0` | Matches the request clientReference, if provided. | Same as `request`. |
| **status** | `'succeeded', 'failed'` | **required** | `*` | Indicates the disposition of the response with respect to the request. `'succeeded'` means the response is for a successful request, while `failed` means the response is for an unsuccessful request. | the only two possible values are `succeeded` and `failed`. A timeout is a `failed` response, with a `message` indicating a timeout. |
| **messages** | `message[]`| **required** | `*` | An array of `message` objects generated during the request. | Messages are intended to be tracked for the developer, though clients may chose some to return to the end user, when appropriate. |
| **timestamp** | `string(25)` | **required** | `*` | [ISO 8601][iso8601] Date format; e.g., `'2014-06-25T04:08:22Z'` |  |
| **requestReceived** | `string(25)` | _optional_  | `^1.0.0` | [ISO 8601][iso8601] Date format; e.g., `'2014-06-25T04:08:22Z'` | This is only useful if you don't have the initial request. Given our logging system, that should never happen. |
| **requestChain** | `string[]` | _optional_  | `>=2.0.0` | An array of request ids that were made to fulfill this request. The order is a best-attempt to maintain the real-world request order. | This is primarily a logging and tracing feature. |
#### `message`

Represents a response message, of a particular type.

| Field | Type | Required | Protocol | Semantics | Comments |
| ------ | ------ | ------ |:------:| ------ | ------ |
| **severity** | `'error', 'warning', 'info', 'debug'` | **required** | `*` | The severity of this message. |  |
| **text** | `string(unlimited)` | **required** | `*` | The message text. | This should be a human readable representation of the message. |
| **code** | `string(255)` | _optional_ | `*` | An opaque string representing the type of message this is. | This is the machine readable version of `text`. |
| **name** | `string(255)` | _optional_ | `*` | The name of this type of message. (Typically the error class name.) | This can be used by the code to pick the right class to serialize this message into. |
| **stack** | `string(unlimited)` | _optional_ | `*` | A stack trace of the message, if applicable | Never show expose this to a user! |
| **details** | `object` or `array` | _optional_ | `*` | An unschema'd object or array of additional details that could be useful to a developer. | Never show expose this to a user! This should never be used or relied on by code. |

## Actions

Actions are much like `requests`, but they are 'fire and forget'. An action is presumed to be either allowed or ignored; either way the client isn't interested in what happens with the action.

An `action` only has one part: the `Action Envelope`.

### Action Envelope 

| Field | Type | Required | Protocol | Semantics | Comments |
| ------ | ------ | ------ |:------:| ------ | ------ |
| **id** | `string(60)` | **required** | `>=2.0.0` | Must be a unique id, preferably a GUID/UUID. Assigned by communication library; should be treated as a unique, opaque value, and should be unique for all time. | This is for matching messages, and avoiding duplicates. |
| **context** | `string(255)` | **required** | `>=2.0.0` | A conceptual grouping of related operations. | This is most similar to a resource in REST terms. |
| **operation** | `string(255)` | **required** | `>=2.0.0` | The specific operation being requested. | This is most similar to an endpoint in REST terms. |
| **payload** | `object` | **required** | `>=2.0.0` | Ad object of arguments required by the operation. (Each request payload has it's own schema.) | Previous versions allowed this to be any valid type, but we have canonized it being an object, now. |
| **userToken** | `jwt` | _optional_ | `>=2.0.0` | Identifies a request as a specific user. Must follow [RFC7519][rfc7519]. | This should be preferred. |
| **metadata** | `object` | _optional_ | `>=2.0.0` | Used to store meaningful, unschema'd tracking information. | We use this for developer-based tracking and analytics, but should never be used or relied on by code. |
| **timestamp** | `string(25)` | **required** | `>=2.0.0` | [ISO 8601][iso8601] Date format; e.g., `'2014-06-25T04:08:22Z'` |  |

## Events

Events are like `actions`, but they are from service to client. They are not delivered over a traditional message queue, but rather are delivered over a PubSub.

| Field | Type | Required | Protocol | Semantics | Comments |
| ------ | ------ | ------ |:------:| ------ | ------ |
| **id** | `string(60)` | **required** | `>=2.0.0` | Must be a unique id, preferably a GUID/UUID. Assigned by communication library; should be treated as a unique, opaque value, and should be unique for all time. | This is for matching messages, and avoiding duplicates. |
| **name** | `string(255)` | **required** | `>=2.0.0` | This is the canonical name for an event. | This is most similar to a resource in REST terms. |
| **payload** | `object` | **required** | `>=2.0.0` | Ad object of arguments required by the operation. (Each request payload has it's own schema.) | Previous versions allowed this to be any valid type, but we have canonized it being an object, now. |
| **timestamp** | `string(25)` | **required** | `>=2.0.0` | [ISO 8601][iso8601] Date format; e.g., `'2014-06-25T04:08:22Z'` |  |

[semver]: https://semver.org/
[iso8601]: https://en.wikipedia.org/wiki/ISO_8601
[rfc2822]: https://tools.ietf.org/html/rfc2822#section-3.4.1
[rfc7519]: https://tools.ietf.org/html/rfc7519

<style>
.table td:nth-child(2) {
    white-space: nowrap;
}
</style>
